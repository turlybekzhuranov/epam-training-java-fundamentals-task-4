import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args){
        int matrixDimension = getMatrixDimension();
        int columnNumberToSort = getColumnNumberToSort(matrixDimension);
        int[][] randomMatrix = getRandomMatrix(matrixDimension);
        int[][] sortedMatrixByColumnNumber = getSortedMatrixByColumnNumber(randomMatrix, columnNumberToSort);
        int[] firstPositiveNumbers = getFirstPositiveNumbersInEachRow(sortedMatrixByColumnNumber);
        int[] secondPositiveNumbers = getSecondPositiveNumebersInEachRow(sortedMatrixByColumnNumber);
        int[] sumBetweenFirstAndSecondPositiveNumbers = getSumOfNumbersBetweenFirstAndSecondPositiveElementsInEachRow(sortedMatrixByColumnNumber, firstPositiveNumbers, secondPositiveNumbers);
        printSortedMatrixByColumnNUmber(sortedMatrixByColumnNumber, columnNumberToSort);
        printSumOfNumbersBetweenTwoPositiveElementsInEachRow(randomMatrix, sumBetweenFirstAndSecondPositiveNumbers, firstPositiveNumbers, secondPositiveNumbers);

        int maxElementOFMatrix = getMaxElementFromMatrix(sortedMatrixByColumnNumber);
        int[][] matrixWithoutRowsAndColumnsOfMaxElement = getMatrixWithoutRowsAndColumnsOfMaxElement(sortedMatrixByColumnNumber, maxElementOFMatrix);
        printMatrixWithoutRowsAndColumnsOfMaxElement(matrixWithoutRowsAndColumnsOfMaxElement, maxElementOFMatrix);
    }

    static boolean isNumeric(String string){
        return string.matches("-?\\d+(\\.\\d+)?");
    }
    static String getRequestedLine(String requestMessage){
        Requester requester = new Requester();
        return requester.requestLine(requestMessage);
    }
    static int getMatrixDimension(){
        String requestedLine = getRequestedLine("Enter matrix size, to get random matrix. (integer number more than 2)").trim();
        return (isNumeric(requestedLine) && Integer.parseInt(requestedLine) > 1) ? Integer.parseInt(requestedLine) : getMatrixDimension();
    }


    static int getColumnNumberToSort(int dimension){
        String requestedLine = getRequestedLine("Enter matrix's column number, to sort that column in ascending order: (integer number from 1 to " + dimension + ")");
        if (!isNumeric(requestedLine) || Integer.parseInt(requestedLine) < 1 || Integer.parseInt(requestedLine) > dimension){
            return getColumnNumberToSort(dimension);
        }
        return (Integer.parseInt(requestedLine)) - 1;
    }

    static int getRandom(int min, int max){
        Random random = new Random();
        return min+Math.abs(random.nextInt()%((max-min)+1));
    }
    static int[][] getRandomMatrix(int dimension){
        int[][] randomMatrix = new int[dimension][dimension];
        for (int i=0; i<dimension; i++){
            for (int j=0; j<dimension; j++){
                randomMatrix[i][j] = getRandom(-1000, 1000);
            }
        }
        return randomMatrix;
    }

    static int[][] getSortedMatrixByColumnNumber(int[][] matrix, int column){
        for (int i=0; i<matrix.length; i++){
            for (int j=i+1; j<matrix.length; j++){
                if (matrix[i][column] > matrix[j][column]){
                    for (int k=0; k<matrix.length; k++){
                        int temp = matrix[i][k];
                        matrix[i][k] = matrix[j][k];
                        matrix[j][k] = temp;
                    }
                }
            }
        }
        return matrix;
    }
    static void printSortedMatrixByColumnNUmber(int[][] matrix, int column){
        System.out.println("Sorted matrix in ascending order of values of the elements of the " + (column+1) + " column");
        System.out.println("-----------------------------------------------------------------");
        for (int[] rows : matrix) {
            System.out.println(Arrays.toString(rows));
        }
        System.out.println("-----------------------------------------------------------------");
    }

    static int[] getFirstPositiveNumbersInEachRow(int[][] matrix){
        int[] firstPositiveNumber = new int[matrix.length];
        for (int i = 0; i<matrix.length; i++){
            firstPositiveNumber[i] = -1;
            for (int j=0; j<matrix.length; j++){
                if (matrix[i][j] > 0){
                    firstPositiveNumber[i] = matrix[i][j];
                    break;
                }
            }
        }
        return firstPositiveNumber;
    }
    static int[] getSecondPositiveNumebersInEachRow(int[][] matrix){
        int[] secondPositiveNumber = new int[matrix.length];
        for (int i=0; i<matrix.length; i++){
            int positiveNumberCount = 0;
            secondPositiveNumber[i] = -1;
            for (int j=0; j <matrix.length; j++){
                if (matrix[i][j] > 0){
                    positiveNumberCount++;
                }
                if (positiveNumberCount == 2){
                    secondPositiveNumber[i] = matrix[i][j];
                    break;
                }
            }
        }
        return secondPositiveNumber;
    }

    static int[] getSumOfNumbersBetweenFirstAndSecondPositiveElementsInEachRow(int[][] matrix, int[] firstPositiveNumber, int[] secondPositiveNumber){
        int[] sumOfNumbersBetweenFirstAndSecondPositiveNumbers = new int[matrix.length];
        for (int i=0; i<matrix.length; i++){
            sumOfNumbersBetweenFirstAndSecondPositiveNumbers[i] = 0;
            if (secondPositiveNumber[i] != -1){
                int firstPositiveNumberColumn = -1, secondPositiveNumberColumn = -1;
                for (int j=0; j<matrix.length; j++){
                    if (matrix[i][j] == firstPositiveNumber[i]){
                        firstPositiveNumberColumn = j + 1;
                    } else if (matrix[i][j] == secondPositiveNumber[i]){
                        secondPositiveNumberColumn= j;
                    }
                }
                for (int k=firstPositiveNumberColumn; k<secondPositiveNumberColumn; k++){
                    sumOfNumbersBetweenFirstAndSecondPositiveNumbers[i] = matrix[i][k];
                }
            }
        }
        return sumOfNumbersBetweenFirstAndSecondPositiveNumbers;
    }
    static void printSumOfNumbersBetweenTwoPositiveElementsInEachRow(int[][] matrix, int[] sum, int[] firstPositiveNumber, int[] secondPositiveNumber){
        System.out.println("Sum of elements between two positive numbers in each row");
        System.out.println("-----------------------------------------------------------------");
        for (int i=0; i<matrix.length; i++){
            if (secondPositiveNumber[i] != -1){
                System.out.println((i + 1) + ". Elements: " + firstPositiveNumber[i] + ", " + secondPositiveNumber[i] + "; Sum: " + sum[i]);
            }
            else {
                System.out.println((i+1) + ". row has no two positive numbers");
            }
        }
        System.out.println("-----------------------------------------------------------------");
    }

    static int getMaxElementFromMatrix(int[][] matrix){
        int maxElement = matrix[0][0];
        for (int[] rows : matrix) {
            for (int columns: rows) {
                if (maxElement < columns) {
                    maxElement = columns;
                }
            }
        }
        return maxElement;
    }
    static int[][] getMatrixWithoutRowsAndColumnsOfMaxElement(int[][] matrix, int maxElement){
        int[] rowToRemove = new int[matrix.length];
        int[] columnToRemove = new int[matrix.length];
        for (int i=0; i<rowToRemove.length; i++){
            rowToRemove[i] = -1;
            columnToRemove[i] = -1;
        }

        int maxElementCount = 0;

        for (int i=0; i<matrix.length; i++){
            for (int j=0; j<matrix.length; j++){
                if (maxElement == matrix[i][j]){
                    rowToRemove[i] = i;
                    columnToRemove[j] = j;
                    maxElementCount++;
                }
            }
        }

        int[][] matrixWithoutRowsAndColumnsOfMaxElement = new int[matrix.length-maxElementCount][matrix.length-maxElementCount];
        int rowNumber = 0;
        for (int i=0; i<matrix.length; i++){
            if (i == rowToRemove[i]){
                continue;
            }
            int columnNumber = 0;
            for (int j=0; j<matrix.length; j++){
               if (j == columnToRemove[j]){
                   continue;
               }
               matrixWithoutRowsAndColumnsOfMaxElement[rowNumber][columnNumber] = matrix[i][j];
               ++columnNumber;
            }
            ++rowNumber;
        }
        return matrixWithoutRowsAndColumnsOfMaxElement;
    }

    static void printMatrixWithoutRowsAndColumnsOfMaxElement(int[][] matrix, int maxElement){
        System.out.println("Maximum element of matrix is " + maxElement + ". And all rows and columns containing maximum element removed from the matrix.\nResulting matrix is below");
        System.out.println("-----------------------------------------------------------------");
        for (int[] rows: matrix){
            System.out.println(Arrays.toString(rows));
        }
        System.out.println("-----------------------------------------------------------------");
    }
}
